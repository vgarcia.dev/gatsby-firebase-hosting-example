<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
# Deploy a gatsby site to firebase

An example repo

## Live Deployed url

[https://gatsby-hosting-example.firebaseapp.com/](https://gatsby-hosting-example.firebaseapp.com/)


## How re-create

### Setup Gatsby

- Create new gatsby site using [gatsby new](https://www.gatsbyjs.org/docs/quick-start/)


### Setup Firebase 

- Create firebase account. Likely with your gmail.
- Install [firebase cli](https://firebase.google.com/docs/cli#setup_update_cli)
- `firebase login`, `firebase login --reauth`, depending (401?)
- `firebase init`, select `hosting`.
  - Choose project name
  - Continue initing project with defaults
- Override the `firebase.json` with the [customized firebase json](firebase.json) in this project.


## Deploying to firebase

### Build gatsby

```bash
npx gatsby build
```

### Use firebase-cli to deploy

```bash
firebase deploy -m ${someMessage}
```

e.g.:

```bash
firebase deploy -m "I am the first deploy"
```